### What is this repository for? ###
To provide ready made reusable components for android application development. This includes

* Grid Data generator with given data
* Url component that will perform async calling and response handling

### How do I get set up? ###

* Setup project as library

### Contribution guidelines ###
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Owner: pramodnanduri@gmail.com
* Chief contributor: karthikeyan