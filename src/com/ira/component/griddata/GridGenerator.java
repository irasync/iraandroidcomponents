package com.ira.component.griddata;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View.OnLongClickListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.ira.manufacture.R;

public abstract class GridGenerator {
	public void generate(final GridData data) {
		TableLayout table = getTableLayout();
		setDefaultTableProperties(table);
		int rows = data.getRowCount();
		if (data.getHeaders() != null) {
			TableRow headerRow = constructHeaderRow(getContext(), data.getDisplayHeaders());
			setDefaultRowViewProperties(headerRow);
			table.addView(headerRow);
			//constructRowLine();
		}
		for (int i = 0; i < rows; i++) {			
			final TableRow row = constructTableRow(getContext(), i, data);
			setDefaultRowViewProperties(row);
			table.addView(row);
			//constructRowLine();
			row.setOnLongClickListener(getOnLongclickListener(row));
		}
	}

	/*private void constructRowLine() {
		TableRow row = new TableRow(getContext());
		row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		View v = new View(getContext());
		v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1, 1.0f));
		v.setPadding(0, 0, 0, 2);
		getTableLayout().addView(v);
		
	}*/

	TableRow constructTableRow(Context context, int rowId, GridData data) {
		TableRow row = new TableRow(context);
		row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		int cols = data.getColumnCount(rowId);
		for (int j = 0; j < cols; j++) {
			TextView tv = constructTextView(getContext(), String.valueOf(data.getData(rowId, j)), false);
			row.addView(tv);
		}
		return row;
	}

	TableRow constructHeaderRow(Context context, String[] headers) {
		TableRow row = new TableRow(context);
		row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		int cols = headers.length;
		for (int j = 0; j < cols; j++) {
			TextView tv = constructTextView(getContext(), headers[j], false);
			tv.setTypeface(null, Typeface.BOLD);
			row.addView(tv);
		}
		return row;
	}

	TextView constructTextView(Context context, String data, boolean isHeader) {
		TextView tv = new TextView(context);		
		setDefaultTextViewProperties(tv);
		tv.setText(data);
		return tv;
	}

	void setDefaultTableProperties(TableLayout table) {
		table.setBackgroundColor(Color.rgb(0,0,0));
	}

	void setDefaultRowViewProperties(TableRow row) {
		row.setPadding(0, 10, 0, 10);		
		row.setBackgroundColor(Color.rgb(255, 255, 255));
	}

	void setDefaultTextViewProperties(TextView tv) {
		tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		tv.setBackgroundResource(R.drawable.cell_shape);
		tv.setGravity(Gravity.CENTER);
		tv.setTextSize(18);		
	}

	public abstract TableLayout getTableLayout();

	public abstract Context getContext();

	public abstract OnLongClickListener getOnLongclickListener(TableRow row);
	
	public interface GridData {

		public String[] getHeaders();
		
		public String[] getDisplayHeaders();

		public Object getData(int row, int column);

		public int getRowCount();

		public int getColumnCount(int row);
		
		public void onDeleteClick(TableRow row);
	}
}
