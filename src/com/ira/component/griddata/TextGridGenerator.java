package com.ira.component.griddata;

import android.content.Context;
import android.widget.TableLayout;

public abstract class TextGridGenerator extends GridGenerator {

	Context ctx;
	TableLayout tableLayout;

	public TextGridGenerator(Context ctx, TableLayout tableLayout) {
		this.ctx = ctx;
		this.tableLayout = tableLayout;
	}

	@Override
	public TableLayout getTableLayout() {
		return tableLayout;
	}

	@Override
	public Context getContext() {
		return ctx;
	}

}
