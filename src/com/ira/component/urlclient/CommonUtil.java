//$Id$
package com.ira.component.urlclient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class CommonUtil {

	public static String convertInputStreamToString(InputStream inputStream) throws Exception {
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String line = "";
			String result = "";

			while ((line = bufferedReader.readLine()) != null) {
				result += line;
			}
			return result;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public static void closeConnection(HttpURLConnection conn) throws Exception {
		try {
			if (conn != null) {
				conn.disconnect();
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public static void closeInputStream(InputStream is) throws Exception {
		try {
			if (is != null) {
				is.close();
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
