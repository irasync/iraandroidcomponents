package com.ira.component.urlclient;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONUrlHandler extends UrlHandler {

	JSONResponseHandler responseHandler;

	public JSONUrlHandler(String url, HashMap<String, String> params, JSONResponseHandler responseHandler) {
		super(url, params);
		this.responseHandler = responseHandler;
	}

	HttpResponseHandler getResponseHandler() {
		return responseHandler;
	}

	public abstract static class JSONResponseHandler implements HttpResponseHandler {

		public abstract void executeAction(JSONArray jarray);

		@Override
		public void execute(Object response) {
			String resp = (String) response;
			try {
				JSONArray array = null;
				if (resp != null) {
					array = new JSONArray(resp);
				}
				executeAction(array);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					executeAction(new JSONArray().put(new JSONObject().put("0", resp.substring(1, 40))));
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	
	public abstract static class JSONObjectResponseHandler extends JSONResponseHandler{

		public abstract void executeAction(JSONObject jobj);

		@Override
		public void execute(Object response) {
			String resp = (String) response;
			try {
				JSONObject jobj = null;
				if (resp != null) {
					jobj = new JSONObject(resp);
				}
				executeAction(jobj);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					executeAction(new JSONObject("Error " + resp.substring(1, 40)));
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

}
