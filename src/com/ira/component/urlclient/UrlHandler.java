package com.ira.component.urlclient;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map.Entry;

import android.os.AsyncTask;

public abstract class UrlHandler {
	private static final int CONNECT_TIMEOUT = 15000;
	private static final int READ_TIMEOUT = 15000;
	
	abstract HttpResponseHandler getResponseHandler();

	private String url;
	private HashMap<String, String> params;

	public UrlHandler(String url, HashMap<String, String> params) {
		this.url = url;
		this.params = params;
	}

	public void doGet() {
		new HttpAsyncTask(HttpMethodType.GET).execute();
	}

	public void doPost() {
		new HttpAsyncTask(HttpMethodType.POST).execute();
	}

	public class HttpAsyncTask extends AsyncTask<String, Void, String> {

		private HttpMethodType method;

		public HttpAsyncTask(HttpMethodType method) {
			this.method = method;
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				return sendRequest();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			getResponseHandler().execute(result);
		}

		String sendRequest() throws Exception {
			return sendHttpRequest(method.getName(), params);
		}

		String sendHttpRequest(String method, HashMap<String, String> params) throws Exception {
			HttpURLConnection conn = null;
			InputStream stream = null;
			try {
				StringBuilder postData = new StringBuilder();
				if (!(params.isEmpty())) {
					for (Entry<String, String> m : params.entrySet()) {
						if (postData.length() != 0) {
							postData.append('&');
						}
						postData.append(URLEncoder.encode(m.getKey(), EncodingScheme.UTF8.getName()));
						postData.append('=');
						postData.append(URLEncoder.encode(String.valueOf(m.getValue()), "UTF-8"));
					}
					url = url + postData;
//					byte[] postDataBytes = postData.toString().getBytes("UTF-8");
//					conn.getOutputStream().write(postDataBytes);
				}
				URL u = new URL(url);
				conn = (HttpURLConnection) u.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setRequestMethod(method);
				conn.setConnectTimeout(CONNECT_TIMEOUT);
				conn.setReadTimeout(READ_TIMEOUT);
				conn.setRequestProperty("Content-Type", "text/json");
				stream = conn.getInputStream();
				return CommonUtil.convertInputStreamToString(stream);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw ex;
			} finally {
				CommonUtil.closeInputStream(stream);
				CommonUtil.closeConnection(conn);
			}
		}
	}

	public static interface HttpResponseHandler {
		void execute(Object response);
	}

	public static enum HttpMethodType {
		GET, POST;

		public String getName() {
			return this.name();
		}

	}

	public static enum EncodingScheme {
		UTF8;

		public String getName() {
			return this.name();
		}
	}

}
